import re
import sys, os
import operator

reg=re.compile(r"^([A-Z]\w+\s[A-Z]\w+).+?(\d+).+?(\d)")
record=[]

#insert player to the list if the player is not in the list
def insert(name, bat, hit):
	player = check_exist(name)
	if player is None:
		NewPlayer = {'name':name, 'bat': bat, 'hit':hit}
		record.append(NewPlayer)
	else:
		player['bat'] =player['bat']+bat
		player['hit'] =player['hit']+hit

#check if the player is already in the lst
def check_exist(name):
	for r in record:
		if r['name']==name:
			return r

#get player infor from the line from file
#using regular expression
def getinfo(data):
	match = reg.match(data)
	if match is not None:
		gname=match.group(1)
		gbat=float(match.group(2))
		ghit=float(match.group(3))
		insert(gname, gbat, ghit)


if len(sys.argv) < 2:
        sys.exit("Usage: <script file> <input file>")

filename = sys.argv[1]

if not os.path.exists(filename):
        sys.exit("Error: No such file: '%s'" % sys.argv[1])

f=open(filename)
for line in f:
	getinfo(line)

#calculate batting average
for i in record:
	result = round(i['hit']/i['bat'],3)
	i['result']=result

#sort the list by batting average
sorted_record = sorted(record, key=operator.itemgetter('result'), reverse=True)

#print output
for i in sorted_record:
	print i['name'], ':', "%.3f" %i['result']

f.close()